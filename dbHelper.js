const config = require('./config')
const constant = require('./constant')

const { Sequelize, QueryTypes } = require('sequelize');


const sequelize = new Sequelize(constant.database.name, config.dbConnection.user, config.dbConnection.password, {
    host: config.dbConnection.host,
    dialect: 'mysql',
    logging: false,
    operatorsAliases: 0

});


const execute = async (query) => {
    try {
        await sequelize.authenticate();
        const rows = await sequelize.query(query, { type: "SELECT" });

        return Promise.resolve(rows)
    } catch (error) {
        return Promise.reject(error);
    }
}

const executeOne = async (query) => {
    try {
        await sequelize.authenticate();

        const rows = await sequelize.query(query, { type: QueryTypes.SELECT });
        return Promise.resolve(rows.length > 0 ? rows[0] : null)
    } catch (error) {
        return Promise.reject(error);
    }
}


const insert = async (tableName, param) => {
    try {

        await sequelize.authenticate();

        let paramKeys = Object.keys(param);
        const query = `
      INSERT INTO ${constant.database.name}.${tableName}
      (${paramKeys.join(', ')}) 
      VALUES
      ( ${paramKeys.map((i, ind) => `"${param[i]}"`).join(', ')} )
      `;

        const rows = await sequelize.query(query, { type: QueryTypes.INSERT });

        return Promise.resolve(rows);
    } catch (error) {
        return Promise.reject(error)
    }
}


const insertMany = async (tableName, param) => {
    try {
        await sequelize.authenticate();

        let itemKeys = Object.keys(param[0])
        let query = "INSERT INTO " + constant.database.name + '.' + tableName + " (" + itemKeys.join(',') + ") VALUES ";//( '" + itemValues.join("', '") + "' ) ";
        for (let obj of param) {
            let itemValues = []
            itemKeys.forEach(function (item) {
                let val = obj[item]
                if (val) {
                    val = val.toString()
                    val = val.replace(/'/g, "''")
                }

                itemValues.push(val);
            });
            query += " (\'" + itemValues.join('\',\'') + "\'),"
        }
        query = query.slice(0, -1)
        console.log(query)
        const rows = await sequelize.query(query, { type: QueryTypes.INSERT });

        return Promise.resolve(rows);
    } catch (error) {
        return Promise.reject(error);
    }
}



const update = async (tableName, queryParam, updateParam) => {
    try {
        await sequelize.authenticate();

        let queryKeys = Object.keys(queryParam);
        let updateKeys = Object.keys(updateParam);

        let tableToUpdate = [];
        let colTOUpdate = [];
        let colToQuery = [];
        tableToUpdate.push(`UPDATE ${tableName} SET`);

        updateKeys.map((item, index) => {
            const value = updateParam[item];
            const isNull = value === null || value === undefined || value.length === 0;
            const isString = typeof value === 'string';
            const q = `${item}=${isNull ? null : `'${isString ? value.replace(/'/g, '') : value}'`}`;
            colTOUpdate.push(q);
        });

        queryKeys.map((item, index) => {
            const isNull = item === null;
            const q = `${item}=${isNull ? null : `'${queryParam[item]}'`}`;
            colToQuery.push(q);
        });

        const query = tableToUpdate.concat(colTOUpdate.join(', '), "where", colToQuery.join(' and ')).join(' ');
        console.log(query)
        const rows = await sequelize.query(query, { type: QueryTypes.UPDATE });
        return Promise.resolve(rows);
    } catch (error) {
        return Promise.reject(error)
    }
}


module.exports = { execute, executeOne, update, insert, insertMany }