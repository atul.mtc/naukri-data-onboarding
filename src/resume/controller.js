var normalizedPath = require("path").join(__dirname, '10001-20000/Resumes-Index-10001-20000');
var fs = require("fs").promises;
var path = require('path');
const { insert, insertMany, update } = require("../../dbHelper");
const constant = require("../../constant");
const readResume = async () => {
    const files = await fs.readdir(normalizedPath)
    console.log(files.length)

    for (let i = 0; i < files.length; i++) {
        if (files[i].split('_')[1] <= 10000 && files[i].split('_')[1] >= 1) {
            console.log(files[i].split('_')[1])
            await update(constant.table.naukriCSV, { SrNo: files[i].split('_')[1], resume_mapped: 0 }, { resume_path: `10001-20000/Resumes-Index-10001-20000/${files[i]}`, resume_mapped: 1 })
        }
        else {
            console.log(files[i].split('_')[1])

        }

    }


}



const insertCSV = async (json) => {
    const model = (json.map((item) => {
        return {
            "SrNo": item["Sr.No"],
            "Name": item["Name"],
            "Email_ID": item["Email ID"],
            "Phone_Number": item["Phone Number"],
            "Current_Location": item["Current Location"],
            "Department": item["Department"],
            "Role": item["Role"],
            "Industry": item["Industry"],
            "Key_Skills": item["Key Skills"],
            "Notice_Period": item["Notice Period"],
            "Date_of_Birth": item["Date of Birth"],
            "Gender": item["Gender"],
            "Marital_Status": item["Marital Status"],
            "Pin_Code": item["Pin Code"],
        }
        // await insert(constant.table.naukriCSV, model)

    }))
    console.log(model)
    await insertMany(constant.table.naukriCSV, model)
}





module.exports = { readResume, insertCSV }