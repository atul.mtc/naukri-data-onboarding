const { readResume, insertCSV } = require("./controller");

const constant = require("../../constant");
const serviceResponse = require("../../serviceResponse");

const routes = (app) => {

    app.get('/read-resume', (req, res) => {
        readResume().then((result) => {
            res.send(serviceResponse({
                result, status: constant.apiStatus.success,
                allowed: true,
            }))
        }).catch((error) => {
            res.send(serviceResponse({ error, status: constant.apiStatus.failed }))
        })
    })

    app.post('/insert-csv', (req, res) => {
        insertCSV(req.body.json).then((result) => {
            res.send(serviceResponse({
                result, status: constant.apiStatus.success,
                allowed: true,
            }))
        }).catch((error) => {
            res.send(serviceResponse({ error, status: constant.apiStatus.failed }))
        })
    })
}

module.exports = routes;