const config = require("../../config");
const constant = require("../../constant");
const { executeOne, insert, update } = require("../../dbHelper");
const { candidateRegistrationModel } = require("./model");
const moment = require('moment')
const S3 = require("aws-sdk/clients/s3")
const { s3Configs } = config;
const { region, accessKeyId, secretAccessKey, bucketName } = s3Configs;
const s3 = new S3({
    region,
    accessKeyId,
    secretAccessKey,
});
const fs = require('fs')


const onBoard = async () => {
    const getData = await executeOne(`SELECT * FROM ${constant.table.naukriCSV} WHERE ONBOARDED = 0 AND DUPLICATE = 'N' AND resume_path IS NOT NULL  limit 1`)
    try {
        const isExist = await executeOne(`SELECT count(*) as count FROM qa_rozgar_db.rg_global_master_candidatedetails AS jr WHERE LOWER(jr.EMAIL_ID) = '${getData.Email_ID}' OR PHONENO = '${getData.Phone_Number}' `);
        if (isExist.count > 0) {
            await update(constant.table.naukriCSV, { ID: getData.ID }, { DUPLICATE: 'Y' })
            console.log('onboard exist');
            // onboard_rozgar_resume()
            onBoard()
            return false
        }
        else {

            getData.Password = generatePassword()
            getData.fullName = getData.Name
            getData.firstName = getData.Name.split(' ')[0]
            getData.secondName = getData.Name.split(' ')[1]
            getData.email = getData.Email_ID
            getData.mobile = getData.Phone_Number.substr(getData.Phone_Number.length - 10); 
            const model = candidateRegistrationModel(getData)
            const res = await insert('rg_global_master_candidatedetails', model)
            await update(constant.table.naukriCSV, { ID: getData.ID }, { ONBOARDED: 1 })
            let req = {
                CANDIDATE_ID: res[0],
                REWARDS_NAME: 'SIGNUP',
                NARRATION: ' Points to sign up',
                POINTS_FOR_ID: res[0]
            }
            await update_reward_points(req);

            let fileName = getData.resume_path

            let candidateDir = `resume/${res[0]}`;
            let file = {
                path: "./src/resume/" + getData.resume_path,
                fileName: fileName
            }

            await uploadFile(file, candidateDir)
            var dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
            let model2 = {
                RESUME_FILE: fileName,
                RESUME_UPDATE_TIME: dateTime
            }
            await update('rg_global_master_candidatedetails', { CANDIDATE_ID: res[0] }, model2)


            await insert('rg_global_candidate_update_resume_time', { CANDIDATE_ID: res[0], RESUME_FILE: fileName })


            let rewards = await executeOne(`SELECT * FROM qa_rozgar_db.rg_global_master_rewardspoint WHERE REWARDS_NAME = 'UPDATE_RESUME'`)
            if (rewards !== null) {
                let isExist = await executeOne(`SELECT * FROM qa_rozgar_db.rg_global_rewards_transactionhistory WHERE CANDIDATE_ID = '${res[0]}' and REWARDS_POINT_ID='${rewards.REWARDS_POINT_ID}'`)
                if (isExist == null) {
                    let req = {
                        CANDIDATE_ID: res[0],
                        REWARDS_NAME: 'UPDATE_RESUME',
                        NARRATION: ' Points to update resume',
                        POINTS_FOR_ID: res[0]
                    }

                    await update_reward_points(req);
                }
            }

            console.log('done')
            onBoard()
        }
    } catch (err) {
        console.log(err)
    }

}
const generatePassword = () => {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}


const uploadFile = (file, folder) => {
    const fileStream = fs.createReadStream(file.path);
    const uploadParams = {
        Bucket: 's3rozgar' + '/' + folder,
        Body: fileStream,
        Key: file.fileName,
        ACL: "public-read",
    };

    return s3.upload(uploadParams).promise();
};

const update_reward_points = async (req) => {
    let rewards = await executeOne(`SELECT * FROM qa_rozgar_db.rg_global_master_rewardspoint WHERE REWARDS_NAME='${req.REWARDS_NAME}'`)
    if (rewards !== null) {
        let isExist = await executeOne(`SELECT * FROM qa_rozgar_db.rg_global_rewards_transactionhistory WHERE CANDIDATE_ID = '${req.CANDIDATE_ID}' and REWARDS_POINT_ID='${rewards.REWARDS_POINT_ID}' and POINTS_FOR_ID='${req.POINTS_FOR_ID}'`)
        if (isExist === null) {
            let model = {
                CANDIDATE_ID: req.CANDIDATE_ID,
                REWARDS_POINT_ID: rewards.REWARDS_POINT_ID,
                POINTS_FOR_ID: req.POINTS_FOR_ID,
                POINT_EARNED: rewards.REWARDS_VALUE,
                NARRATION: rewards.REWARDS_VALUE + req.NARRATION,
                ADDED_BY: req.CANDIDATE_ID,
                MODIFIED_BY: req.CANDIDATE_ID,
                TXN_DATE: moment().format("YYYY-MM-DD")

            }
            await insert('rg_global_rewards_transactionhistory', model)
        }
    }


}

module.exports = { onBoard }