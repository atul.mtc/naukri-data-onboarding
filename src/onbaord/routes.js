const { onBoard } = require("./controller");

const constant = require("../../constant");
const serviceResponse = require("../../serviceResponse");

const routes = (app) => {


    app.post('/onboard', (req, res) => {
        onBoard().then((result) => {
            res.send(serviceResponse({
                result, status: constant.apiStatus.success,
                allowed: true,
            }))
        }).catch((error) => {
            res.send(serviceResponse({ error, status: constant.apiStatus.failed }))
        })
    })
}

module.exports = routes;