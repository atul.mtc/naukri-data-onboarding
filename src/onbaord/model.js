const pkg = require('bcryptjs');
const { hashSync, genSaltSync } = pkg;
const candidateRegistrationModel = (body) => {
    const hashed = hashSync(body.Password, genSaltSync(10))
    let fullName = body.firstName?.trim() + ' ' + body.secondName ? body.secondName?.trim() : ''
    const model = {
        CANDIDATE_NAME: fullName,
        FIRST_NAME: body.firstName?.trim(),
        SECOND_NAME: body.secondName ? body.secondName?.trim() : '',
        EMAIL_ID: body.email?.trim(),
        PHONENO: body.mobile?.trim(),
        PASSWORD: hashed,
        WORK_STATUS: 'E',
        ACCEPT_TNC: 'Y',
        STATUS: 'U',
        IS_EMAIL_VERIFIED: 'N',
        IS_PHONE_VERIFIED: 'N',
        ONBOARDED_THROUGH_NAUKRI: 'Y'

    }
    let isNull = body.SORT_NUMBER === null || body.SORT_NUMBER === undefined || body.SORT_NUMBER.length === 0;
    model.SORT_NUMBER = isNull ? 1 : Number(body.SORT_NUMBER);
    let isNull2 = body.PassCode === null || body.PassCode === undefined || body.PassCode.length === 0;
    model.PASSCODE = isNull2 ? '' : Number(body.PassCode);

    return (
        model
    )
}

module.exports = { candidateRegistrationModel }