const resume = require('../resume/routes')
const onBoard = require('../onbaord/routes')

function routes(app) {
    resume(app)
    onBoard(app)
}
module.exports = routes