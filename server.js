const express = require('express');
const fs = require('fs');
const config = require('./config');
const cluster = require("cluster");
const totalCPUs = require("os").cpus().length;
const routes = require('./src/routes/index')
const cors = require('cors')

const app = express();
app.use('*', cors())
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: "50mb" }));
routes(app)

app.get("/", (req, res) => {
    res.send("Hello World!");
});

app.listen(config.PORT, () => {
    console.log(`App listening on port ${config.PORT}`);
});