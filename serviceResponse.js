
const serviceResponse = (body) => {
    const model = {
        status: body.status,
        result: body.result ? body.result : null,
        error: body.error ? body.error.message : null,
        version: '1.0',
        allowed: body.allowed,
        messageCode: body.messageCode
    }

    return model;
};

module.exports = serviceResponse